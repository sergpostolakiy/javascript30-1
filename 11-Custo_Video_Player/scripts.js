//1 - находим наши элементы
const player = document.querySelector('.player');
const video = player.querySelector('.viewer');
const progress = player.querySelector('.progress');
const progressBar = player.querySelector('.progress__filled');
const toggle = player.querySelector('.toggle');
const skipButtons = player.querySelectorAll('[data-skip]');
const ranges = player.querySelectorAll('.player__slider');
//2 - создаём функции
function togglePlay() {
   /* старт / стоп видео */
   const method = video.paused ? 'play' : 'pause';
   video[method]();
   // if (video.paused) {
   //    video.play()
   // } else {
   //    video.pause();
   // }
}

function updateButton() {
   /*смена кнопки при нажатии*/
   const icon = this.paused ? '►' : '❚ ❚';
   toggle.textContent = icon;
   // console.log(icon);
}

function skip() {
   /*кнопки перемотки вперед/назад*/
   console.log(this.dataset.skip);
   video.currentTime += parseFloat(this.dataset.skip);
}

function handleRangeUpdate() {
   /*регулировка ползунков мышью*/
   video[this.name] = this.value;
   console.log(this.name);
   console.log(this.value);
}

function handleProgress() {
   /*изменение мышью времени просмотра*/
   const percent = (video.currentTime / video.duration) * 100;
   progressBar.style.flexBasis = `${percent}%`;
}

function scrub(e) {
   /*перемещает по полосе времени*/
   const scrubTime = (e.offsetX / progress.offsetWidth) * video.duration;
   video.currentTime = scrubTime;
   console.log(e);
}
//3 - подключаем слушателей событий
video.addEventListener('click', togglePlay); //слушает нажатие на экран и запускает/останавливает воспроизведение
video.addEventListener('play', updateButton); //слушает нажатие на 'play' и меняет кнопку
video.addEventListener('pause', updateButton); //слушает нажатие на 'pause' и меняет кнопку
video.addEventListener('timeupdate', handleProgress); // слушает событие обновления времени

toggle.addEventListener('click', togglePlay); //слушает нажатие на кнопку и запускает/останавливает воспроизведение
skipButtons.forEach(button => button.addEventListener('click', skip));
ranges.forEach(range => range.addEventListener('change', handleRangeUpdate));
ranges.forEach(range => range.addEventListener('mousemove', handleRangeUpdate));

let mousedown = false;
progress.addEventListener('click', scrub); //по клику мышкой запускает scrub
progress.addEventListener('mousemove', (e) => mousedown && scrub(e)); //по перетаскиванию с нажатой кнопкой мыши запускает scrub
progress.addEventListener('mousedown', () => mousedown = true); //фиксирует прогресс только при нажатой клавише мыши
progress.addEventListener('mouseup', () => mousedown = false); //отменяет прогресс если клавиша мыши не нажата