let countdown;
const timerDisplay = document.querySelector('.display__time-left');
const endTime = document.querySelector('.display__end-time');
const buttons = document.querySelectorAll('[data-time]')

function timer(seconds) {
   clearInterval(countdown); //очистить все запущеные таймеры
   const now = Date.now();
   const then = now + seconds * 1000;
   displayTimeLeft(seconds); //показывает начальное время отсчета
   displayEndTime(then);

   countdown = setInterval(() => {
      const secondsLeft = Math.round((then - Date.now()) / 1000);
      // проверяем, должны ли мы остановить это!
      if (secondsLeft < 0) {
         clearInterval(countdown); //очистить все запущеные таймеры
         return;
      }
      // показать это
      displayTimeLeft(secondsLeft);
   }, 1000);
}

function displayTimeLeft(seconds) {
   const minutes = Math.floor(seconds / 60); //считает оставшиеся минуты
   const remainderSeconds = seconds % 60; //сичтает оставшиеся секунды
   const display = `${minutes}:${remainderSeconds < 10 ? '0' : ''}${remainderSeconds}`;
   document.title = display;
   timerDisplay.textContent = display;

}

function displayEndTime(timestamp) {
   const end = new Date(timestamp);
   const hour = end.getHours();
   const adjustedHour = hour > 12 ? hour - 12 : hour;
   const minutes = end.getMinutes();
   endTime.textContent = `Вернуться в ${adjustedHour}:${minutes < 10 ? '0' : ''}${minutes}`;
}

function startTimer() {
   const seconds = parseInt(this.dataset.time);
   timer(seconds);
   // console.log(seconds);
}

buttons.forEach(button => button.addEventListener('click', startTimer));

document.customForm.addEventListener('submit', function (e) {
   e.preventDefault();
   const mins = this.minutes.value;
   console.log(mins);
   timer(mins * 60);
   this.reset();
})